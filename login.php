<?php require('header.php'); ?>
<section id="login">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="log-frm">
                        <form action="">
                            <div class="card">
                                <div class="card-header">
                                    <div class="log-logo"><img src="assets/img/logo.png" alt=""></div>
                                    <div class="log-txt text-right">
                                        <h6>Login</h6>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="single-form">
                                        <input id="User" type="text" placeholder="User Name">
                                    </div>
                                    <div class="single-form">
                                        <input id="phone" type="text" placeholder="Password">
                                    </div>
                                    <div class="single-btn">
                                        <button>Login</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require('footer.php'); ?>
