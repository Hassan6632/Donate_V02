<?php include('header.php'); ?>

<!--    [ Strat Section Title Area]-->
<section id="welcome" class="">
    <div class="overlay">
        <div class="wel-come-areas owl-carousel">
            <div class="welcomte-content d-table">
                <div class="welcome-txt welcome-bg d-table-cell">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 text-center">
                                <h2>Learn. <span>Collaborate.</span> Validate.</h2>
                                <h5>Join 47 million students.</h5>
                                <div class="welcome-btn">
                                    <a href="">Start a free trial</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="welcomte-content d-table">
                <div class="welcome-txt welcome-bg-02 d-table-cell">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 text-center">
                                <h2>Learn. <span>Collaborate.</span> Validate.</h2>
                                <h5>Join 47 million students.</h5>
                                <div class="welcome-btn">
                                    <a href="">Start a free trial</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<section id="blog-type-project">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Projects</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <a href="#" class="project-detail-content">
                        <div class="single-project-image">
                            <img src="assets/img/03.jpg" alt="">
                        </div>
                        <div class="project-detail-txt">
                            <div class="project-detail-funding">
                                <span>Funding</span> <span class="text-right"><i class="icofont icofont-heart"></i></span>
                            </div>
                            <div class="project-heading-txt">
                                <h4>Dreamlight: The World's Smartest Sleep Mask</h4>
                            </div>
                            <div class="project-progress">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                                    <p>70%</p>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="pro-target-money">
                                            <h5>Target : $78,950</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="pro-target-money text-right">
                                            <h5>Collected : $55,265</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="contribut">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="conti">
                                                <div class="pro-target-money">
                                                    <h5>Contributors : 78,950</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="conti">
                                                <div class="pro-target-money">
                                                    <button>Donate Now</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="#" class="project-detail-content">
                        <div class="single-project-image">
                            <img src="assets/img/03.jpg" alt="">
                        </div>
                        <div class="project-detail-txt">
                            <div class="project-detail-funding">
                                <span>Funding</span> <span class="text-right"><i class="icofont icofont-heart"></i></span>
                            </div>
                            <div class="project-heading-txt">
                                <h4>Dreamlight: The World's Smartest Sleep Mask</h4>
                            </div>
                            <div class="project-progress">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                                    <p>70%</p>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="pro-target-money">
                                            <h5>Target : $78,950</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="pro-target-money text-right">
                                            <h5>Collected : $55,265</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="contribut">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="conti">
                                                <div class="pro-target-money">
                                                    <h5>Contributors : 78,950</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="conti">
                                                <div class="pro-target-money">
                                                    <button>Donate Now</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="#" class="project-detail-content">
                        <div class="single-project-image">
                            <img src="assets/img/03.jpg" alt="">
                        </div>
                        <div class="project-detail-txt">
                            <div class="project-detail-funding">
                                <span>Funding</span> <span class="text-right"><i class="icofont icofont-heart"></i></span>
                            </div>
                            <div class="project-heading-txt">
                                <h4>Dreamlight: The World's Smartest Sleep Mask</h4>
                            </div>
                            <div class="project-progress">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                                    <p>70%</p>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="pro-target-money">
                                            <h5>Target : $78,950</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="pro-target-money text-right">
                                            <h5>Collected : $55,265</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="contribut">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="conti">
                                                <div class="pro-target-money">
                                                    <h5>Contributors : 78,950</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="conti">
                                                <div class="pro-target-money">
                                                    <button>Donate Now</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<section id="subscribe">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <form action="">
                        <div class="subscribtion-frm">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Your Email Address" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><button>Subscribe</button></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<section id="mission">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <div class="mission-menu">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Mission</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Vision</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">History</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    01
                    <div class="mission-content">
                        <div class="mission-title text-center">
                            <h4>Our Mission</h4>
                        </div>
                        <div class="mission-txt">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    02
                    <div class="mission-content">
                        <div class="mission-title text-center">
                            <h4>Our Mission</h4>
                        </div>
                        <div class="mission-txt">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    03
                    <div class="mission-content">
                        <div class="mission-title text-center">
                            <h4>Our Mission</h4>
                        </div>
                        <div class="mission-txt">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem itaque distinctio dolore velit consectetur explicabo maxime saepe adipisci ex pariatur nisi, omnis a eligendi provident natus id soluta laudantium accusamus.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>
