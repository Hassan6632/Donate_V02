<?php require('header.php'); ?>
<section id="search">
    <div class="section-padding">
        <div class="container">
            <div class="donate-money-list">
                <div class="card">
                    <div class="donate-money-list-title text-center">
                        <h3>Donors</h3>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mission-menu">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Requests</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">My Accepts</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#history" role="tab" aria-controls="profile" aria-selected="false">History</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6">

                        </div>
                    </div>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row justify-content-end">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p>Filter</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                          <option value="volvo">Volvo</option>
                                          <option value="saab">Saab</option>
                                          <option value="mercedes">Mercedes</option>
                                          <option value="audi">Audi</option>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                          <option value="volvo">Volvo</option>
                                          <option value="saab">Saab</option>
                                          <option value="mercedes">Mercedes</option>
                                          <option value="audi">Audi</option>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="singleofild">
                                                <button><i class="icofont icofont-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            01
                            <div class="mission-content">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Campaign</th>
                                                    <th scope="col">Item</th>
                                                    <th scope="col">Quentity</th>
                                                    <th scope="col">dONOR</th>
                                                    <th scope="col">Address</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td colspan="2">Larry the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row justify-content-end">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p>Filter</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                          <option value="volvo">Volvo</option>
                                          <option value="saab">Saab</option>
                                          <option value="mercedes">Mercedes</option>
                                          <option value="audi">Audi</option>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                          <option value="volvo">Volvo</option>
                                          <option value="saab">Saab</option>
                                          <option value="mercedes">Mercedes</option>
                                          <option value="audi">Audi</option>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="singleofild">
                                                <button><i class="icofont icofont-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            02
                            <div class="mission-content">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Campaign</th>
                                                    <th scope="col">Item</th>
                                                    <th scope="col">Quentity</th>
                                                    <th scope="col">dONOR</th>
                                                    <th scope="col">Address</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td colspan="2">Larry the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="history" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row justify-content-end">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p>Filter</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                          <option value="volvo">Volvo</option>
                                          <option value="saab">Saab</option>
                                          <option value="mercedes">Mercedes</option>
                                          <option value="audi">Audi</option>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                          <option value="volvo">Volvo</option>
                                          <option value="saab">Saab</option>
                                          <option value="mercedes">Mercedes</option>
                                          <option value="audi">Audi</option>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="singleofild">
                                                <button><i class="icofont icofont-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            03
                            <div class="mission-content">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Campaign</th>
                                                    <th scope="col">Item</th>
                                                    <th scope="col">Quentity</th>
                                                    <th scope="col">dONOR</th>
                                                    <th scope="col">Address</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td colspan="2">Larry the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<?php require('footer.php'); ?>
