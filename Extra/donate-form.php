<?php require('header.php'); ?>
<!--    [ Strat Section Area]-->
<section id="donate-frm">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="mission-menu text-center">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#dn-money" role="tab" aria-controls="home" aria-selected="true">Money</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#dn-goods" role="tab" aria-controls="profile" aria-selected="false">Goods</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="dn-money" role="tabpanel" aria-labelledby="home-tab">
                            01
                            <div class="mission-content">
                                <div class="mission-title text-center">
                                    <h4><i class="icofont icofont-money-bag"></i> Donate Moeny </h4>
                                </div>
                                <div class="donate-frm">
                                    <form action="">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="singleofild">
                                                    <label for="">Sending Amount</label>
                                                    <input type="text" placeholder="2356 ">
                                                </div>
                                                <div class="singleofild">
                                                    <div class="frm-pic">
                                                        <img src="assets/img/03.jpg" alt="">
                                                        <img src="assets/img/03.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="singleofild-radio">
                                                    <p>Sending Amount</p>
                                                    <div class="single-fild-part">
                                                        <input type="radio" name="Campaign" id="radion"> <label for="radion">Organization</label>
                                                    </div>
                                                    <div class="single-fild-part">
                                                        <input type="radio" name="Campaign" id="radion2"><label for="radion2">Campaign</label>
                                                    </div>
                                                    <select>
                                                      <option value="volvo">Volvo</option>
                                                      <option value="saab">Saab</option>
                                                      <option value="mercedes">Mercedes</option>
                                                      <option value="audi">Audi</option>
                                                    </select>
                                                </div>
                                                <div class="singleofild">
                                                    <label for="">Payment Method</label>
                                                    <select>
                                                      <option value="volvo">Volvo</option>
                                                      <option value="saab">Saab</option>
                                                      <option value="mercedes">Mercedes</option>
                                                      <option value="audi">Audi</option>
                                                    </select>
                                                </div>
                                                <div class="singleofild">
                                                    <label for="">Sender Number</label>
                                                    <input type="text" placeholder="2356 ">
                                                </div>
                                                <div class="singleofild">
                                                    <label for="">Receiver Number</label>
                                                    <input type="text" placeholder="2356 ">
                                                </div>
                                                <div class="singleofild">
                                                    <label for="">Transaction ID*</label>
                                                    <input type="text" placeholder="2356 ">
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row justify-content-center">
                                            <div class="col-lg-3">
                                                <div class="singleofild">
                                                    <button>Confirm</button>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="dn-goods" role="tabpanel" aria-labelledby="profile-tab">
                            02
                            <div class="mission-content">
                                <div class="mission-title text-center">
                                    <h4><i class="icofont icofont-box"></i> Donate Goods</h4>
                                </div>
                                <div class="mission-txt">
                                    <div class="donate-frm">
                                        <form action="">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="singleofild">
                                                        <div class="frm-pic">
                                                            <img src="assets/img/03.jpg" alt="">
                                                            <img src="assets/img/03.jpg" alt="">
                                                            <img src="assets/img/03.jpg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="singleofild-radio">
                                                        <p>Sending Amount</p>
                                                        <div class="single-fild-part">
                                                            <input type="radio" name="Campaign" id="radion"> <label for="radion">Organization</label>
                                                        </div>
                                                        <div class="single-fild-part">
                                                            <input type="radio" name="Campaign" id="radion2"><label for="radion2">Campaign</label>
                                                        </div>
                                                        <select>
                                                      <option value="volvo">Volvo</option>
                                                      <option value="saab">Saab</option>
                                                      <option value="mercedes">Mercedes</option>
                                                      <option value="audi">Audi</option>
                                                    </select>
                                                    </div>
                                                    <div class="singleofild">
                                                        <label for="">Category</label>
                                                        <select>
                                                      <option value="volvo">Volvo</option>
                                                      <option value="saab">Saab</option>
                                                      <option value="mercedes">Mercedes</option>
                                                      <option value="audi">Audi</option>
                                                    </select>
                                                    </div>
                                                    <div class="singleofild">
                                                        <label for="">Location</label>
                                                        <textarea name="" id="" cols="30" rows="2" placeholder="Location"></textarea>
                                                    </div>
                                                    <div class="singleofild photo">
                                                        <label for="">Photo</label>
                                                        <button>Upload mage <i class="icofont icofont-image"></i></button>
                                                        <input type="file">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row justify-content-center">
                                                <div class="col-lg-3">
                                                    <div class="singleofild">
                                                        <button>Confirm</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<?php require('footer.php'); ?>
