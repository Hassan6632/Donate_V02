<?php require("header.php"); ?>
<!--    [ Strat Section Title Area]-->
<section id="welcome" class="detail-slide">
    <div class="overlay">
        <div class="wel-come-areas owl-carousel">
            <div class="welcomte-content d-table">
                <div class="welcome-txt welcome-bg d-table-cell">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 text-center">
                                <h2>Learn. <span>Collaborate.</span> Validate.</h2>
                                <h5>Join 47 million students.</h5>
                                <div class="welcome-btn">
                                    <a href="">Start a free trial</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="welcomte-content d-table">
                <div class="welcome-txt welcome-bg-02 d-table-cell">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 text-center">
                                <h2>Learn. <span>Collaborate.</span> Validate.</h2>
                                <h5>Join 47 million students.</h5>
                                <div class="welcome-btn">
                                    <a href="">Start a free trial</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->

<!--    [ Strat Section Title Area]-->
<section id="detail-info">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="donate-money-list">
                        <div class="card">
                            <div class="donate-money-list-title">
                                <h3>Donors</h3>
                            </div>
                            <div class="single-money-info">
                                <h5>Jhon Doue $54288<span><strong>Occopation:</strong>Business Man</span></h5>
                            </div>
                            <div class="single-money-info">
                                <h5>Jhon Doue $54288<span><strong>Occopation:</strong>Business Man</span></h5>
                            </div>
                            <div class="single-money-info">
                                <h5>Jhon Doue $54288<span><strong>Occopation:</strong>Business Man</span></h5>
                            </div>
                            <div class="single-money-info">
                                <h5>Jhon Doue $54288<span><strong>Occopation:</strong>Business Man</span></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="donate-money-list">
                        <div class="card">
                            <div class="project-detail-txt">
                                <div class="donate-money-list-title text-center">
                                    <h3>Help Helpless Peoples</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis tempore voluptas illum, labore aperiam autem!</p>
                                </div>
                                <div class="project-heading-txt">
                                    <h4>Earned: $54815156</h4>
                                    <h4>Contributor: 9815156</h4>
                                </div>
                                <div class="project-progress">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                                        <p>70%</p>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="pro-target-money">
                                                <h5>Target : $78,950</h5>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="pro-target-money text-right">
                                                <h5>Day : 65</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="caimpaign-company-info">
                                        <h5>Logo Five Star</h5>
                                        <h5>Website: <a href="#">Plab.com</a></h5>
                                        <h5>Address: 21/A Zigatola, Dhaka 1204</h5>
                                        <h5>Contact: +8801825573355</h5>
                                    </div>
                                    <div class="contribut">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="singleofild">
                                                    <a href="#">Donate</a>
                                                </div>
                                            </div>
                                            <!-- <div class="col-lg-6">
                                                <div class="singleofild">
                                                    <button>Volunteering</button>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="donate-money-list">
                <div class="card">
                    <div class="donate-money-list-title text-center">
                        <h3>Donors</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="single-money-info">
                                        <h5>Jhon Doue $54288<span><strong>Occopation:</strong>Business Man</span></h5>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="single-money-info">
                                        <h5><span><mark>2 min Left</mark></span></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="single-money-info">
                                        <h5>Jhon Doue $54288<span><strong>Occopation:</strong>Business Man</span></h5>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="single-money-info">
                                        <h5><span><mark>2 min Left</mark></span></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="single-money-info">
                                        <h5>Jhon Doue $54288<span><strong>Occopation:</strong>Business Man</span></h5>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="single-money-info">
                                        <h5><span><mark>2 min Left</mark></span></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="single-money-info">
                                        <h5>Jhon Doue $54288<span><strong>Occopation:</strong>Business Man</span></h5>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="single-money-info">
                                        <h5><span><mark>2 min Left</mark></span></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="single-money-info">
                                        <h5>Jhon Doue $54288<span><strong>Occopation:</strong>Business Man</span></h5>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="single-money-info">
                                        <h5><span><mark>2 min Left</mark></span></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="single-money-info">
                                        <h5>Jhon Doue $54288<span><strong>Occopation:</strong>Business Man</span></h5>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="single-money-info">
                                        <h5><span><mark>2 min Left</mark></span></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->

<!--    [ Strat Section Title Area]-->
<!--    [Finish Section Title Area]-->

<!--    [ Strat Section Title Area]-->
<!--    [Finish Section Title Area]-->

<!--    [ Strat Section Title Area]-->
<!--    [Finish Section Title Area]-->

<?php require("footer.php"); ?>
