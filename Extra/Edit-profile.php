<?php require('header.php'); ?>
<section id="edit-pro">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <div class="nav flex-column nav-pills pro-side-btn" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Personal Information</a>
                        <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile Picture</a>
                        <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Subscriptions</a>
                        <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Change Password</a>
                        <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-login" role="tab" aria-controls="v-pills-settings" aria-selected="false">Log Out</a>

                    </div>
                </div>
                <div class="col-9">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                            <div class="pro-form">
                                <form action="">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="single-form">
                                                <label for="name">Name</label>
                                            </div>
                                            <div class="single-form">
                                                <label for="phone">Phone</label>
                                            </div>
                                            <div class="single-form">
                                                <label for="address">Address</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="single-form">
                                                <input id="name" type="text" placeholder="Name">
                                            </div>
                                            <div class="single-form">
                                                <input id="phone" type="text" placeholder="Phone">
                                            </div>
                                            <div class="single-form">
                                                <textarea id="address" name="" id="" placeholder="Address"></textarea>
                                            </div>
                                            <div class="single-btn">
                                                <button>Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">02
                            <div class="pro-pic">
                                <div class="row justify-content-end">
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="user-pic">
                                                    <img src="assets/img/prof.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="user-pic-up">
                                                    <div class="single-btn pro-pic-gap">
                                                        <button><i class="icofont icofont-image"></i></button>
                                                        <input type="file">
                                                    </div>
                                                    <div class="single-btn">
                                                        <button>Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">03
                            <div class="row justify-content-end">
                                <div class="col-lg-8">
                                    <div class="subsc-form">
                                        <h5>Newslatter Subscription</h5>
                                        <form action="">
                                            <div class="single-form-cus">
                                                <input id="check" type="checkbox" placeholder="Name">
                                                <label for="name">Unscribtion From Email Newslatter</label>
                                            </div>
                                            <div class="single-btn">
                                                <button>Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">04
                            <div class="pro-form">
                                <form action="">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="single-form">
                                                <label for="name">Old Password</label>
                                            </div>
                                            <div class="single-form">
                                                <label for="phone">New Password</label>
                                            </div>
                                            <div class="single-form">
                                                <label for="address">Copnfurm Password</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="single-form">
                                                <input id="name" type="text" placeholder="*******">
                                            </div>
                                            <div class="single-form">
                                                <input id="phone" type="text" placeholder="******">
                                            </div>
                                            <div class="single-form">
                                                <input id="phone" type="text" placeholder="******">
                                            </div>
                                            <div class="single-btn">
                                                <button>Change</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-login" role="tabpanel" aria-labelledby="v-pills-settings-tab">05...</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require('footer.php'); ?>
