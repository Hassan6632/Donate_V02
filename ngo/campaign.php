<?php require('header.php'); ?>
<!--    [ Strat Section Area]-->
<section id="blog-type-project">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Projects</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <a href="#" class="project-detail-content">
                        <div class="single-project-image">
                            <img src="../assets/img/03.jpg" alt="">
                        </div>
                        <div class="project-detail-txt">
                            <div class="project-detail-funding">
                                <span>Funding</span> <span class="text-right"><i class="icofont icofont-heart"></i></span>
                            </div>
                            <div class="project-heading-txt text-center">
                                <h4>Help Poor People</h4>
                            </div>
                            <div class="project-progress">
                                <hr>
                                <div class="contribut">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="conti cam-con">
                                                <div class="pro-target-money">
                                                    <button>Edit</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="conti cam-con">
                                                <div class="pro-target-money">
                                                    <button>Delete</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="#" class="project-detail-content">
                        <div class="single-project-image">
                            <img src="../assets/img/03.jpg" alt="">
                        </div>
                        <div class="project-detail-txt">
                            <div class="project-detail-funding">
                                <span>Funding</span> <span class="text-right"><i class="icofont icofont-heart"></i></span>
                            </div>
                            <div class="project-heading-txt text-center">
                                <h4>Help Poor People</h4>
                            </div>
                            <div class="project-progress">
                                <hr>
                                <div class="contribut">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="conti cam-con">
                                                <div class="pro-target-money">
                                                    <button>Edit</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="conti cam-con">
                                                <div class="pro-target-money">
                                                    <button>Delete</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="#" class="project-detail-content">
                        <div class="single-project-image">
                            <img src="../assets/img/03.jpg" alt="">
                        </div>
                        <div class="project-detail-txt">
                            <div class="project-detail-funding">
                                <span>Funding</span> <span class="text-right"><i class="icofont icofont-heart"></i></span>
                            </div>
                            <div class="project-heading-txt text-center">
                                <h4>Help Poor People</h4>
                            </div>
                            <div class="project-progress">
                                <hr>
                                <div class="contribut">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="conti cam-con">
                                                <div class="pro-target-money">
                                                    <button>Edit</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="conti cam-con">
                                                <div class="pro-target-money">
                                                    <button>Delete</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<?php require('footer.php'); ?>
